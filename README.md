# symbiomatique

> Symbiomatique, your computer in symbiosis with your thoughts

## Usage

``` bash
# Install dependencies
npm install

# Build, watch for changes and run the application
tns run <platform> --bundle

# Build for production
tns build <platform> --bundle

# Build, watch for changes and debug the application
tns debug <platform> --bundle
```

For local deployment you need to adjust config.ts to the backend url.
I had to use ngrok to create a TCP tunnel to my local backend.

Please replace in config.ts:
``` bash
export const baseUrlApi = ' http://backend-url.com'
```