const state = {
    // this toggles the isLogged
    isLogged: false
}

const mutations = {
    // always and only change vuex state through mutations.
    setLoggedIn (state, data) {
        state.isLogged = data
    }
}

const getters = {
    // the 'isLogged' getter will be available to listen to on the front end.
    isLogged: (state) => state.isLogged
}

// export this module.
export default {
    state,
    mutations,
    getters
}

// TODO : Set all user datas (mail, expire time of the token, refresh_roken, username, user_id, role_id, company_id, location_id, firstname, lastname).
// TODO : Keep only user_id + role_id in the payload ?
// https://www.nativescript.org/blog/key-value-local-storage-in-a-vue.js-nativescript-app-with-vuex