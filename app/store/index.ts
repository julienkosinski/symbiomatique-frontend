import Vue from 'nativescript-vue'
import Vuex from 'vuex'

import sideDrawer from './modules/sideDrawer'
import login from './modules/login'

Vue.use(Vuex)

let debug = process.env.NODE_ENV !== 'production'

let store = new Vuex.Store({
    modules: {
        sideDrawer,
        login
    },
    strict: debug
})

Vue.prototype.$store = store

export default store