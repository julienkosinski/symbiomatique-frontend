import axios from 'axios/dist/axios'
import createAuthRefreshInterceptor from 'axios-auth-refresh'
import * as AppSettings from '@nativescript/core/application-settings'
import { baseUrlApi } from './config'

const axiosInstance = axios.create({
    baseURL: baseUrlApi,
    headers: {
        "Content-type": "application/json"
    }
})

function getAccessToken(): string {
    return AppSettings.getString('token')
}

function setAccessToken(token): void {
    AppSettings.setString('token', token)
}

function getRefreshToken(): string {
    return AppSettings.getString('refreshToken')
}

const refreshAuthLogic = failedRequest => axiosInstance.get('/refresh/' + getRefreshToken()).then(tokenRefreshResponse => {
    setAccessToken(tokenRefreshResponse.data.token)
    failedRequest.response.config.headers['Authorization'] = `Bearer ${getAccessToken()}`
    axiosInstance.defaults.headers.common['Authorization'] = `Bearer ${getAccessToken()}`
    return Promise.resolve()
})

createAuthRefreshInterceptor(axiosInstance, refreshAuthLogic)


export default axiosInstance