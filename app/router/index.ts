import Welcome from '../pages/Welcome.vue'
import Connection from '../pages/Connection.vue'
import Questions from '../pages/Questions.vue'
import ListEquipment from '../pages/ListEquipment.vue'
import AddEquipment from '../pages/AddEquipment.vue'

const routes = {
    Welcome,
    Connection,
    Questions,
    ListEquipment,
    AddEquipment,
}

export default routes