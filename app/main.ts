import Vue from 'nativescript-vue';
import routes from './router'
import store from './store'
import sideDrawer from './components/sideDrawer.vue'
import drawerContent from './components/drawerContent.vue'
import axiosInstance from './token'

import VueDevtools from 'nativescript-vue-devtools'
import RadSideDrawer from "nativescript-ui-sidedrawer/vue"
import FilterSelect from "nativescript-filter-select"

if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}

// Prints Vue logs when --env.production is *NOT* set while building
Vue.config.silent = (TNS_ENV === 'production');

// Set up routes as a prototype to use throughout the app.
Vue.prototype.$routes = routes

// https://fr.vuejs.org/v2/cookbook/adding-instance-properties.html#Un-exemple-en-situation-reelle-Remplacer-Vue-Resource-par-Axios
Vue.prototype.$http = axiosInstance

Vue.use(RadSideDrawer)

Vue.registerElement("FilterSelect", () => require("nativescript-filter-select").FilterSelect)

new Vue({
  store,
  render (h) {
    return h(
        sideDrawer,
        [
          h(drawerContent, { slot: 'drawerContent' }),
          h(routes.Welcome, { slot: 'mainContent' })
        ]
    )
  }
}).$start();
